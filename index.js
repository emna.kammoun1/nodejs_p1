const fsPromises = require('fs').promises
const path = require('path')
//meme que fs.readFile('./files/starter.txt','utf8',(err,data) => {

const fileOps = async () => {
    try {
        const data = await fsPromises.readFile(path.join(__dirname,'files','starter.txt'),'utf8')
        console.log(data)
        //delete the first file
        await fsPromises.unlink(path.join(__dirname,'files' , 'starter.txt'))

        await fsPromises.writeFile(path.join(__dirname,'files' , 'promiseWrite.txt'), data)
        await fsPromises.appendFile(path.join(__dirname,'files' , 'promiseWrite.txt'), ' \n \n nice to meet you')
        await fsPromises.rename(path.join(__dirname,'files' , 'promiseWrite.txt'), path.join(__dirname,'files' , 'promiseComplete.txt'))
        const newdata = await fsPromises.readFile(path.join(__dirname,'files','promiseComplete.txt'),'utf8')
        console.log(newdata)
        //    Hi, my name is Emna
        //Hi, my name is Emna 

        //nice to meet you

    }catch (err) {
        console.error(err)
    }

}
fileOps()
/*fs.readFile(path.join(__dirname,'files','starter.txt') ,'utf8',(err,data) => {
    if (err) throw err
    console.log(data)
    //==>Buffer data
   //console.log(data.toString())
})
console.log('hello..') //la precedente est la premiere 


fs.writeFile(path.join(__dirname,'files','reply2.txt') ,'Nice to meet you','utf8',(err,data) => {
    if (err) throw err
    console.log('write compele')
    fs.appendFile(path.join(__dirname,'files','reply2.txt') ,'\n \n Yes it is','utf8',(err,data) => {
        if (err) throw err
        console.log('append compele')
        fs.rename(path.join(__dirname,'files','reply2.txt'),path.join(__dirname,'files','newReply2.txt' ),(err) => {
            if (err) throw err
            console.log('Rename compele')
        })
    
    })
    
})
*/


//error
process.on('uncaughtException' ,err => {
    console.error(`There was an uncaught error: ${error}`)
    process.exit(1)
})